<?php

require_once __DIR__ . '/vendor/autoload.php';

class PdfManager
{
    private $mpdf = null;
    private $htmlTemplate = '';

    public function __construct()
    {
        $tmpPath = __DIR__ . '/tmp';
        if (!is_dir($tmpPath)) {
            mkdir($tmpPath, 0777, true);
        }
        $this->mpdf = new \Mpdf\Mpdf(['tempDir' => $tmpPath]);
    }

    /**
     * создание pdf документа из переданных данных
     *
     * @return string|null
     */
    public function generetePdf(array $pdfData) : ?string
    {
        if (empty($pdfData)) {
            return null;
        }

        $this->htmlTemplate = file_get_contents('HtmlTemplate.html');
        if (empty($this->htmlTemplate)) {
            return null;
        }

        foreach ($pdfData as $key => $data) {
            $this->createData($key, $data);
        }

        return $this->generatePdfFromHtml();
    }

    /**
     * Рекурсия для нескольких уровней вложенности
     *
     * @param int $key
     * @param string|[] $data
     */
    protected function createData($key, $data)
    {
        if (!is_array($data)) {
            $this->htmlTemplate = str_replace("%{$key}%", $data, $this->htmlTemplate);

            return;
        }

        foreach ($data as $innerKey => $value) {
            if (is_array($value)) {
                $this->createData($innerKey, $value);
            } else {
                $this->htmlTemplate = str_replace("%{$key}_{$innerKey}%", $value, $this->htmlTemplate);
            }
        }
    }

    /**
     * Формирование pdf документа их html
     *
     * @param $html
     * @return string|null
     */
    protected function generatePdfFromHtml() : ?string
    {
        $result = null;
        try {
            $this->mpdf->WriteHTML($this->htmlTemplate);
            $result = $this->mpdf->Output(null, 'S');
        } catch (\Exception $e) {
            //пишем в лог, алертим разработчикам
        }

        return $result;
    }
}