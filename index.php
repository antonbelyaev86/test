<?php

require_once 'PdfManager.php';

// Исходные данные
$data = [
    'ownerName' => 'Geesys DTS',
    'chargeNumber' => 'YYYY',
    'requestNumber' => 'XXXX',
    'chargeDate' => '01.01.2018',
    'customer' => [
        'name' => 'ООО «Сила»',
        'inn' => '77777777',
        'kpp' => '77777777',
        'postcode' => '105111',
        'address' => 'Москва, Рублевское шоссе,  д.32,к.2, оф.777',
    ],
    'agent' => [
        'name' => 'ООО «Джи Системс»',
        'inn' => '77777777',
        'kpp' => '77777777',
        'postcode' => '143355',
        'address' => 'Московская область, Балашихинский район, мкр. Янтарный, ул. Кольцевая, д.10, к.3, оф.2',
    ],
    'executor' => [
        'name' => 'ООО «Джи Системс»',
        'inn' => '77777777',
        'kpp' => '77777777',
        'postcode' => '143355',
        'address' => 'Московская область, Балашихинский район, мкр. Янтарный, ул. Кольцевая, д.10, к.3, оф.2',
    ],
    'test' => [
        'test1' => [
            'test2' => 'test data'
        ]
    ],
];

$pdfManager = new PdfManager();
// pdf получаем строкой
$content = $pdfManager->generetePdf($data);
// выводим в браузер для печати
if ($content) {
    header('Content-Type: application/pdf');
    header('Content-Length: '.strlen($content));
    header('Content-disposition: inline; filename="request.pdf');
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    echo $content;
    exit;
} else {
    echo '<!DOCTYPE html><html lang="ru"><body>Что то пошло не так, напишите нам на <a href="mailto:support@email.com">support@email.com</a></body></html>';
}